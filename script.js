
function functionSwitchButton() {


    
    var switchButton = document.getElementById("start-button").value;
    if (switchButton == "start-value") {
        currenttime = new Date();
        getStartHours = currenttime.getHours();
        getStartMinutes = currenttime.getMinutes();
        document.getElementById("start-time-display").innerHTML = getStartHours + ":" + getStartMinutes;
        document.getElementById("start-button").value = "stop-value";
        document.getElementById("start-button").style.backgroundColor = "red";
        document.getElementById("start-button").innerHTML = "<img src='./images/stop.png' class='w-9 h-7 pr-2' alt=''>Stop";
    } else if (switchButton == "stop-value") {
        stopTime = new Date();
        getStopHour = stopTime.getHours();
        getStopMin = stopTime.getMinutes();
        document.getElementById("stop-time-display").innerHTML = getStopHour + ":" + getStopMin;
        hour = getStopHour - getStartHours;
        hourMinTime = hour * 60;
        minute = getStopMin - getStartMinutes;
        totalMinute = minute + hourMinTime;
        document.getElementById("minute-time-display").innerHTML = "" + totalMinute;
        condition();
        document.getElementById("start-button").style.backgroundColor = "orange";
        document.getElementById("start-button").innerHTML = "<img src='./images/bin.png' class='w-9 h-7 pr-2 ' alt=''>Clear";
        document.getElementById("start-button").value = "clear-value";

    } else if (switchButton == "clear-value") {
        document.getElementById("start-time-display").innerHTML = "00:00";
        document.getElementById("stop-time-display").innerHTML = "00:00";
        document.getElementById("minute-time-display").innerHTML = "0";
        document.getElementById("riel-display").innerHTML = "0";
        document.getElementById("start-button").value = "start-value";
        document.getElementById("start-button").style.backgroundColor = "green";
        document.getElementById("start-button").innerHTML = "<img src='./images/play-button.png' class='w-9 h-7 pr-2' alt=''>Start";
    }
}



function condition() {
    if (totalMinute <= 15) {
        document.getElementById("riel-display").innerHTML = "500";
    } else if (totalMinute <= 30) {
        document.getElementById("riel-display").innerHTML = "1000";
    } else if (totalMinute <= 60) {
        document.getElementById("riel-display").innerHTML = "1500";
    } else {
        var overOfMinute = parseInt(totalMinute / 60);
        var numOfSomNalMin = totalMinute % 60;
        var overOfPrice = overOfMinute * 1500;
        if(numOfSomNalMin<=0){
            var price = 0;
        }
        else if (numOfSomNalMin <= 15) { 
            var price = 500;   
        } else if (numOfSomNalMin <= 30) {
            var price = 1000;
        } else if (numOfSomNalMin <= 60) {
            var price = 1500;
        } else {
            var price = 1500;
        }
        var total = overOfPrice + price;
        document.getElementById("riel-display").innerHTML = total;
    }
}

let time = document.getElementById("currentTime");
setInterval(() => {
    let d = new Date();
    time.innerHTML = d.toLocaleString();
}, 0) 
